package com.example.provider.service.impl;

import com.lcxu.common.api.provider.IBookServiceApi;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class BookServiceImpl implements  IBookServiceApi {

    @Override
    public String sayHello() {
        return "hello, dubbo!这是一个简单的dubbo-demo!";
    }
}
