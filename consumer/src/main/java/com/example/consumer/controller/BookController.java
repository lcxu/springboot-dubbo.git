package com.example.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lcxu.common.api.provider.IBookServiceApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book")
public class BookController {

    @Reference
    private IBookServiceApi bookService;

    @GetMapping("/book")
    private String getBook() {
        return this.bookService.sayHello();
    }
}
